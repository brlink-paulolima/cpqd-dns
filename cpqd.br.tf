resource "aws_route53_zone" "cpqd-br" { name = "cpqd.br" }

resource "aws_route53_record" "mx-cpqd-br" {
  zone_id = "${aws_route53_zone.cpqd-br.zone_id}"
  name    = ""
  type    = "MX"
  records = ["0 barao.cpqd.com.br.","0 conde.cpqd.com.br.", "0 duque.cpqd.com.br."]
  ttl = "${var.ttl}"
}

resource "aws_route53_record" "a-www-cpqd-br" {
  zone_id = "${aws_route53_zone.cpqd-br.zone_id}"
  name    = "www"
  type    = "A"
  records = ["177.125.142.49","177.125.142.47","177.125.142.43"]
  ttl     = "${var.ttl}"
}


resource "aws_route53_record" "txt-cpqd-br" {
  zone_id = "${aws_route53_zone.cpqd-br.zone_id}"
  name    = ""
  type    = "TXT"
  records = ["google-site-verification=QZZttpvJuNSfevuXxgJ04xcyf7OpikqLvnjhqTHEchc"]
  ttl     = "${var.ttl}"
}
