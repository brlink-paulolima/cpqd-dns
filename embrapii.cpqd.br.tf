resource "aws_route53_zone" "embrapii-cpqd-br" { name = "embrapii.cpqd.br" }

resource "aws_route53_record" "mx-embrapii-cpqd-br" {
  zone_id = "${aws_route53_zone.embrapii-cpqd-br.zone_id}"
  name    = ""
  type    = "MX"
  records = ["10 ASPMX.L.GOOGLE.COM.","20 ALT1.ASPMX.L.GOOGLE.COM.","20 ALT2.ASPMX.L.GOOGLE.COM.","30 ASPMX2.GOOGLEMAIL.COM.","30 ASPMX3.GOOGLEMAIL.COM."]
  ttl = "3600"
}

resource "aws_route53_record" "txt-embrapii-cpqd-br" {
  zone_id = "${aws_route53_zone.embrapii-cpqd-br.zone_id}"
  name    = ""
  type    = "TXT"
  records = ["google-site-verification=X_H32o0_qEdXHX7FKU2829vTer6u7LTdoynYn5TRokk"]
  ttl = "3600"
  }
