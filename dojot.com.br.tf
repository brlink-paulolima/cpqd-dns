resource "aws_route53_zone" "dojot-com-br" { name = "dojot.com.br" }

resource "aws_route53_record" "dojot-com-br" {
  zone_id = "${aws_route53_zone.dojot-com-br.zone_id}"
  name    = ""
  type    = "MX"
  records = ["20 ALT1.ASPMX.L.GOOGLE.COM.", "10 ASPMX.L.GOOGLE.COM.","20 ALT2.ASPMX.L.GOOGLE.COM.","30 ASPMX2.GOOGLEMAIL.COM.","30 ASPMX3.GOOGLEMAIL.COM."]
  ttl = "3600"
}


resource "aws_route53_record" "a-dojot-com-br" {
  zone_id = "${aws_route53_zone.dojot-com-br.zone_id}"
  name    = ""
  type    = "A"
  records = ["177.125.143.77"]
  ttl = "3600"
}

resource "aws_route53_record" "a-www-dojot-com-br" {
  zone_id = "${aws_route53_zone.dojot-com-br.zone_id}"
  name    = "www"
  type    = "A"
  records = ["177.125.143.77"]
  ttl = "3600"
}


resource "aws_route53_record" "cname-1547277dojot-com-br" {
  zone_id = "${aws_route53_zone.dojot-com-br.zone_id}"
  name    = "1547277"
  type    = "CNAME"
  records = ["sendgrid.net."]
  ttl = "3600"
}
resource "aws_route53_record" "cname-s1-_domainkey-dojot-com-br" {
  zone_id = "${aws_route53_zone.dojot-com-br.zone_id}"
  name    = "s1._domainkey"
  type    = "CNAME"
  records = ["s1.domainkey.u1547277.wl028.sendgrid.net."]
  ttl = "3600"

}
resource "aws_route53_record" "cname-s2-_domainkey-dojot-com-br" {
  zone_id = "${aws_route53_zone.dojot-com-br.zone_id}"
  name    = "s2._domainkey"
  type    = "CNAME"
  records = ["s2.domainkey.u1547277.wl028.sendgrid.net."]
  ttl = "3600"
}

resource "aws_route53_record" "cname-snd-dojot-com-br" {
  zone_id = "${aws_route53_zone.dojot-com-br.zone_id}"
  name    = "snd"
  type    = "CNAME"
  records = ["u1547277.wl028.sendgrid.net."]
  ttl = "3600"
}

resource "aws_route53_record" "cname-t-snd-dojot-com-br" {
  zone_id = "${aws_route53_zone.dojot-com-br.zone_id}"
  name    = "t.snd"
  type    = "CNAME"
  records = ["sendgrid.net."]
  ttl = "3600"
}


resource "aws_route53_record" "txt-_acme-challenge-dojot-com-br-dojot-com-br" {
  zone_id = "${aws_route53_zone.dojot-com-br.zone_id}"
  name    = "_acme-challenge.dojot.com.br.dojot.com.br"
  type    = "TXT"
  records = ["nykqYZG__u-Nqd_3r3sU0igP6rCEbEwKNfkZil-DbuQ"]
  ttl = "3600"
 }

 resource "aws_route53_record" "txt-_acme-challenge-www-dojot-com-br-dojot-com-br" {
  zone_id = "${aws_route53_zone.dojot-com-br.zone_id}"
  name    = "_acme-challenge.www.dojot.com.br.dojot.com.br"
  type    = "TXT"
  records = ["fSL1yLQ0787gxPr1opQUOuz8S6xzeRmHthVIwo71KMM"]
  ttl = "3600"
 }

 resource "aws_route53_record" "txt-dojot-com-br" {
  zone_id = "${aws_route53_zone.dojot-com-br.zone_id}"
  name    = ""
  type    = "TXT"
  records = ["google-site-verification=FsmyCYMXxhzJRB9VZ2xPHDrzANpsILx45OB-E4o-dy4"]
  ttl = "3600"
 }
