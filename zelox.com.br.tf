resource "aws_route53_zone" "zelox-com-br" { name = "zelox.com.br" }

resource "aws_route53_record" "a-www-zelox-com-br" {
  zone_id = "${aws_route53_zone.zelox-com-br.zone_id}"
  name    = "www"
  type    = "A"
  records = ["187.1.141.50"]
  ttl = "${var.ttl}"
}
resource "aws_route53_record" "a-agenda-zelox-com-br" {
  zone_id = "${aws_route53_zone.zelox-com-br.zone_id}"
  name    = "agenda"
  type    = "CNAME"
  records = ["ghs.google.com."]
  ttl = "${var.ttl}"
}

resource "aws_route53_record" "a-docs-zelox-com-br" {
  zone_id = "${aws_route53_zone.zelox-com-br.zone_id}"
  name    = "docs"
  type    = "CNAME"
  records = ["ghs.google.com."]
  ttl = "${var.ttl}"
}

resource "aws_route53_record" "a-mail-zelox-com-br" {
  zone_id = "${aws_route53_zone.zelox-com-br.zone_id}"
  name    = "mail"
  type    = "CNAME"
  records = ["ghs.google.com."]
  ttl = "${var.ttl}"
}

resource "aws_route53_record" "a-sites-zelox-com-br" {
  zone_id = "${aws_route53_zone.zelox-com-br.zone_id}"
  name    = "sites"
  type    = "CNAME"
  records = ["ghs.google.com."]
  ttl = "${var.ttl}"
}

resource "aws_route53_record" "mx-zelox-com-br" {
  zone_id = "${aws_route53_zone.zelox-com-br.zone_id}"
  name    = ""
  type    = "MX"
  records = ["5 ALT2.ASPMX.L.GOOGLE.COM.","5 ALT1.ASPMX.L.GOOGLE.COM.","1 ASPMX.L.GOOGLE.COM.","10 ASPMX2.GOOGLEMAIL.COM.","10 ASPMX3.GOOGLEMAIL.COM."]
  ttl = "${var.ttl}"
}

resource "aws_route53_record" "txt-zelox-com-br" {
  zone_id = "${aws_route53_zone.zelox-com-br.zone_id}"
  name    = ""
  type    = "TXT"
  records = ["v=spf1 include:_spf.google.com ~all","google-site-verification=49UYAs799ZalVOy05QSXEXDcpEUlk-7um6HgSPq9_uo"]
  ttl = "${var.ttl}"
}
