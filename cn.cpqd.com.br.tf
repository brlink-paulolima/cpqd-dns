resource "aws_route53_zone" "cn-cpqd-com-br" { name = "cn.cpqd.com.br" }

resource "aws_route53_record" "mx-cn-cpqd-com-br" {
  zone_id = "${aws_route53_zone.cn-cpqd-com-br.zone_id}"
  name    = ""
  type    = "MX"
  records = [
    "1 ASPMX.L.GOOGLE.COM.","5 ALT1.ASPMX.L.GOOGLE.COM.","5 ALT2.ASPMX.L.GOOGLE.COM.","10 ASPMX2.GOOGLEMAIL.COM.","10 ASPMX3.GOOGLEMAIL.COM.","10 ASPMX4.GOOGLEMAIL.COM.","10 ASPMX5.GOOGLEMAIL.COM."]
  ttl = "${var.ttl}"
}

resource "aws_route53_record" "a-cn-cpqd-com-br" {
  zone_id = "${aws_route53_zone.cn-cpqd-com-br.zone_id}"
  name    = ""
  type    = "A"
  records = ["177.125.142.49","177.125.142.47","177.125.142.43"]
  ttl = "${var.ttl}"
}

resource "aws_route53_record" "txt-cn-cpqd-com-br" {
  zone_id = "${aws_route53_zone.cn-cpqd-com-br.zone_id}"
  name    = ""
  type    = "TXT"
  records = ["v=spf1 include:aspmx.googlemail.com ~all"]
  ttl = "${var.ttl}"
}

resource "aws_route53_record" "cname-agenda-cn-cpqd-com-br" {
  zone_id = "${aws_route53_zone.cn-cpqd-com-br.zone_id}"
  name    = "agenda"
  type    = "CNAME"
  records = ["ghs.google.com."]
  ttl = "${var.ttl}"
}
resource "aws_route53_record" "cname-docs-cn-cpqd-com-br" {
  zone_id = "${aws_route53_zone.cn-cpqd-com-br.zone_id}"
  name    = "docs"
  type    = "CNAME"
  records = ["ghs.google.com."]
  ttl = "${var.ttl}"

}
resource "aws_route53_record" "cname-googleffffffffbeb157b3-cn-cpqd-com-br" {
  zone_id = "${aws_route53_zone.cn-cpqd-com-br.zone_id}"
  name    = "googleffffffffbeb157b3"
  type    = "CNAME"
  records = ["google.com."]
  ttl = "${var.ttl}"
}

resource "aws_route53_record" "cname-inicial-cn-cpqd-com-br" {
  zone_id = "${aws_route53_zone.cn-cpqd-com-br.zone_id}"
  name    = "inicial"
  type    = "CNAME"
  records = ["ghs.google.com."]
  ttl = "${var.ttl}"
}

resource "aws_route53_record" "cname-mail-cn-cpqd-com-br" {
  zone_id = "${aws_route53_zone.cn-cpqd-com-br.zone_id}"
  name    = "mail"
  type    = "CNAME"
  records = ["ghs.google.com."]
  ttl = "${var.ttl}"
}

