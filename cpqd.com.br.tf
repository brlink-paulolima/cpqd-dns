resource "aws_route53_zone" "cpqd-com-br" { name = "cpqd.com.br" }

resource "aws_route53_record" "mx-cpqd-com-br" {
  zone_id = "${aws_route53_zone.cpqd-com-br.zone_id}"
  name    = ""
  type    = "MX"
  records = [
    "10 ASPMX.L.GOOGLE.COM.","20 ALT1.ASPMX.L.GOOGLE.COM.","20 ALT2.ASPMX.L.GOOGLE.COM.","30 ASPMX2.GOOGLEMAIL.COM.","30 ASPMX3.GOOGLEMAIL.COM."]
  ttl = "3600"
}

resource "aws_route53_record" "mx-vento-cpqd-com-br" {
  zone_id = "${aws_route53_zone.cpqd-com-br.zone_id}"
  name    = "vento"
  type    = "MX"
  records = [
    "0 barao.cpqd.com.br.","10 conde.cpqd.com.br.","20 duque.cpqd.com.br."]
  ttl = "${var.ttl}"
}

resource "aws_route53_record" "mx-jira-cpqd-com-br" {
  zone_id = "${aws_route53_zone.cpqd-com-br.zone_id}"
  name    = "jira"
  type    = "MX"
  records = [
    "0 smtp-jira.cpqd.com.br."]
  ttl = "${var.ttl}"
}

resource "aws_route53_record" "mx-issues-cpqd-com-br" {
  zone_id = "${aws_route53_zone.cpqd-com-br.zone_id}"
  name    = "issues"
  type    = "MX"
  records = [
    "0 smtp-jira.cpqd.com.br."]
  ttl = "${var.ttl}"
}

resource "aws_route53_record" "ns-ga-cpqd-com-br" {
  zone_id = "${aws_route53_zone.cpqd-com-br.zone_id}"
  name    = "ga"
  type    = "NS"
  records = [
        "${aws_route53_zone.ga-cpqd-com-br.name_servers.0}",
        "${aws_route53_zone.ga-cpqd-com-br.name_servers.1}",
        "${aws_route53_zone.ga-cpqd-com-br.name_servers.2}",
        "${aws_route53_zone.ga-cpqd-com-br.name_servers.3}"
    ]
  ttl = "${var.ttl}"
}

resource "aws_route53_record" "ns-geb-cpqd-com-br" {
  zone_id = "${aws_route53_zone.cpqd-com-br.zone_id}"
  name    = "geb"
  type    = "NS"
  records = [
        "${aws_route53_zone.geb-cpqd-com-br.name_servers.0}",
        "${aws_route53_zone.geb-cpqd-com-br.name_servers.1}",
        "${aws_route53_zone.geb-cpqd-com-br.name_servers.2}",
        "${aws_route53_zone.geb-cpqd-com-br.name_servers.3}"
    ]
  ttl = "${var.ttl}"
}

resource "aws_route53_record" "ns-visitor-cpqd-com-br" {
  zone_id = "${aws_route53_zone.cpqd-com-br.zone_id}"
  name    = "visitor"
  type    = "NS"
  records = [
        "${aws_route53_zone.visitor-cpqd-com-br.name_servers.0}",
        "${aws_route53_zone.visitor-cpqd-com-br.name_servers.1}",
        "${aws_route53_zone.visitor-cpqd-com-br.name_servers.2}",
        "${aws_route53_zone.visitor-cpqd-com-br.name_servers.3}"
    ]
  ttl = "${var.ttl}"
}

resource "aws_route53_record" "txt-cpqd-com-br" {
  zone_id = "${aws_route53_zone.cpqd-com-br.zone_id}"
  name    = ""
  type    = "TXT"
  records = ["v=spf1 include:_spf.google.com include:spf.mediapost.com.br ip4:208.75.123.0/24 ip4:68.232.200.1 ip4:68.232.200.2 include:_spf.rdstation.com.br ~all","google-site-verification=3Ah-kfXqadVlLi3yTHjuJXjAseJ6i60NFDfmIR-Sqog","MS=80A70B8F02362E1B3ADC13DA91BA927CE8362119"]
  ttl     = "3600"
}

resource "aws_route53_record" "txt-google-_domainkey-cpqd-com-br" {
  zone_id = "${aws_route53_zone.cpqd-com-br.zone_id}"
  name    = "google._domainkey"
  type    = "TXT"
  records = ["v=DKIM1; k=rsa; t=y; p=MIGfMA0GCSqGSIb3DQEBAQUAA4GNADCBiQKBgQCBq6+kYRxWcAGFDQBEsux7ozVWYoqUBftmLVs8joIEPrkrHzlpEqQnkcTG0dQbz81KjGWo2QVTXe9gbtWlSmEwlE+ETlwX/SAE6WlmP70FnlIExz55nUkvJk/qM4JgWxYZytgsdRSW0REFMEkEFZP/wD+HolzG6edZMGAt6vd7mQIDAQAB"]
  ttl     = "3600"
}

resource "aws_route53_record" "a-acesso-cpqd-com-br" {
  zone_id = "${aws_route53_zone.cpqd-com-br.zone_id}"
  name    = "acesso"
  type    = "A"
  records = ["192.168.11.2"]
  ttl = "${var.ttl}"
}

resource "aws_route53_record" "a-antifraude-cpqd-com-br" {
  zone_id = "${aws_route53_zone.cpqd-com-br.zone_id}"
  name    = "antifraude"
  type    = "A"
  records = ["177.125.143.77"]
  ttl = "${var.ttl}"
}

resource "aws_route53_record" "a-antifrauderede-cpqd-com-br" {
  zone_id = "${aws_route53_zone.cpqd-com-br.zone_id}"
  name    = "antifrauderede"
  type    = "A"
  records = ["177.125.143.77"]
  ttl = "${var.ttl}"
}

resource "aws_route53_record" "a-apoyo-cpqd-com-br" {
  zone_id = "${aws_route53_zone.cpqd-com-br.zone_id}"
  name    = "apoyo"
  type    = "A"
  records = ["177.125.143.77"]
  ttl = "${var.ttl}"
}

resource "aws_route53_record" "a-assistenteavisa-cpqd-com-br" {
  zone_id = "${aws_route53_zone.cpqd-com-br.zone_id}"
  name    = "assistenteavisa"
  type    = "A"
  records = ["177.125.143.77"]
  ttl = "${var.ttl}"
}

resource "aws_route53_record" "a-av-dev-cpqd-com-br" {
  zone_id = "${aws_route53_zone.cpqd-com-br.zone_id}"
  name    = "av-dev"
  type    = "A"
  records = ["177.125.143.77"]
  ttl = "${var.ttl}"
}

resource "aws_route53_record" "a-av-cpqd-com-br" {
  zone_id = "${aws_route53_zone.cpqd-com-br.zone_id}"
  name    = "av"
  type    = "A"
  records = ["177.125.143.77"]
  ttl = "${var.ttl}"
}

resource "aws_route53_record" "a-awswebh-cpqd-com-br" {
  zone_id = "${aws_route53_zone.cpqd-com-br.zone_id}"
  name    = "awswebh"
  type    = "A"
  records = ["177.125.143.77"]
  ttl = "${var.ttl}"
}

resource "aws_route53_record" "a-biblio-cpqd-com-br" {
  zone_id = "${aws_route53_zone.cpqd-com-br.zone_id}"
  name    = "biblio"
  type    = "A"
  records = ["177.125.143.77"]
  ttl = "${var.ttl}"
}

resource "aws_route53_record" "a-bicorp-cpqd-com-br" {
  zone_id = "${aws_route53_zone.cpqd-com-br.zone_id}"
  name    = "bicorp"
  type    = "A"
  records = ["177.125.143.77"]
  ttl = "${var.ttl}"
}

resource "aws_route53_record" "a-biometriadeface-cpqd-com-br" {
  zone_id = "${aws_route53_zone.cpqd-com-br.zone_id}"
  name    = "biometriadeface"
  type    = "A"
  records = ["177.125.143.77"]
  ttl = "${var.ttl}"
}

resource "aws_route53_record" "a-biometriadevoz-cpqd-com-br" {
  zone_id = "${aws_route53_zone.cpqd-com-br.zone_id}"
  name    = "biometriadevoz"
  type    = "A"
  records = ["177.125.143.77"]
  ttl = "${var.ttl}"
}

resource "aws_route53_record" "a-biometriavoz-cpqd-com-br" {
  zone_id = "${aws_route53_zone.cpqd-com-br.zone_id}"
  name    = "biometriavoz"
  type    = "A"
  records = ["177.125.143.77"]
  ttl = "${var.ttl}"
}

resource "aws_route53_record" "a-biometricface-cpqd-com-br" {
  zone_id = "${aws_route53_zone.cpqd-com-br.zone_id}"
  name    = "biometricface"
  type    = "A"
  records = ["177.125.143.77"]
  ttl = "${var.ttl}"
}

resource "aws_route53_record" "a-bivendas-cpqd-com-br" {
  zone_id = "${aws_route53_zone.cpqd-com-br.zone_id}"
  name    = "bivendas"
  type    = "A"
  records = ["177.125.143.77"]
  ttl = "${var.ttl}"
}

resource "aws_route53_record" "a-boletoseguro-cpqd-com-br" {
  zone_id = "${aws_route53_zone.cpqd-com-br.zone_id}"
  name    = "boletoseguro"
  type    = "A"
  records = ["177.125.143.77"]
  ttl = "${var.ttl}"
}

resource "aws_route53_record" "a-brarep-cpqd-com-br" {
  zone_id = "${aws_route53_zone.cpqd-com-br.zone_id}"
  name    = "brarep"
  type    = "A"
  records = ["177.125.143.77"]
  ttl = "${var.ttl}"
}

resource "aws_route53_record" "a-braseg-cpqd-com-br" {
  zone_id = "${aws_route53_zone.cpqd-com-br.zone_id}"
  name    = "braseg"
  type    = "A"
  records = ["177.125.143.77"]
  ttl = "${var.ttl}"
}

resource "aws_route53_record" "a-buscadordsb-cpqd-com-br" {
  zone_id = "${aws_route53_zone.cpqd-com-br.zone_id}"
  name    = "buscadordsb"
  type    = "A"
  records = ["177.125.143.77"]
  ttl = "${var.ttl}"
}

resource "aws_route53_record" "a-canais-cpqd-com-br" {
  zone_id = "${aws_route53_zone.cpqd-com-br.zone_id}"
  name    = "canais"
  type    = "A"
  records = ["34.230.219.235"]
  ttl = "${var.ttl}"
}

resource "aws_route53_record" "a-cc-report-cpqd-com-br" {
  zone_id = "${aws_route53_zone.cpqd-com-br.zone_id}"
  name    = "cc-report"
  type    = "A"
  records = ["177.125.143.77"]
  ttl = "${var.ttl}"
}

resource "aws_route53_record" "a-chesf-demo-cpqd-com-br" {
  zone_id = "${aws_route53_zone.cpqd-com-br.zone_id}"
  name    = "chesf-demo"
  type    = "A"
  records = ["177.125.143.77"]
  ttl = "${var.ttl}"
}

resource "aws_route53_record" "a-coleta-cpqd-com-br" {
  zone_id = "${aws_route53_zone.cpqd-com-br.zone_id}"
  name    = "coleta"
  type    = "A"
  records = ["177.125.143.77"]
  ttl = "${var.ttl}"
}

resource "aws_route53_record" "a-coletabiometrica-cpqd-com-br" {
  zone_id = "${aws_route53_zone.cpqd-com-br.zone_id}"
  name    = "coletabiometrica"
  type    = "A"
  records = ["177.125.143.77"]
  ttl = "${var.ttl}"
}

resource "aws_route53_record" "a-coletabra-cpqd-com-br" {
  zone_id = "${aws_route53_zone.cpqd-com-br.zone_id}"
  name    = "coletabra"
  type    = "A"
  records = ["34.200.153.11"]
  ttl = "${var.ttl}"
}

resource "aws_route53_record" "a-coletavoz-cpqd-com-br" {
  zone_id = "${aws_route53_zone.cpqd-com-br.zone_id}"
  name    = "coletavoz"
  type    = "A"
  records = ["177.125.143.77"]
  ttl = "${var.ttl}"
}

resource "aws_route53_record" "a-componenteseguro-cpqd-com-br" {
  zone_id = "${aws_route53_zone.cpqd-com-br.zone_id}"
  name    = "componenteseguro"
  type    = "A"
  records = ["177.125.143.77"]
  ttl = "${var.ttl}"
}

resource "aws_route53_record" "a-confluence-cpqd-com-br" {
  zone_id = "${aws_route53_zone.cpqd-com-br.zone_id}"
  name    = "confluence"
  type    = "A"
  records = ["177.125.143.77"]
  ttl = "${var.ttl}"
}

resource "aws_route53_record" "a-conformidade-cpqd-com-br" {
  zone_id = "${aws_route53_zone.cpqd-com-br.zone_id}"
  name    = "conformidade"
  type    = "A"
  records = ["177.125.143.77"]
  ttl = "${var.ttl}"
}

resource "aws_route53_record" "a-contactcenter-cpqd-com-br" {
  zone_id = "${aws_route53_zone.cpqd-com-br.zone_id}"
  name    = "contactcenter"
  type    = "A"
  records = ["177.125.143.77"]
  ttl = "${var.ttl}"
}

resource "aws_route53_record" "a-contato-cpqd-com-br" {
  zone_id = "${aws_route53_zone.cpqd-com-br.zone_id}"
  name    = "contato"
  type    = "A"
  records = ["177.125.143.77"]
  ttl = "${var.ttl}"
}

resource "aws_route53_record" "a-cpqd-im-cpqd-com-br" {
  zone_id = "${aws_route53_zone.cpqd-com-br.zone_id}"
  name    = "cpqd-im"
  type    = "A"
  records = ["34.226.103.208"]
  ttl = "${var.ttl}"
}

resource "aws_route53_record" "a-cpqd-itsm-cpqd-com-br" {
  zone_id = "${aws_route53_zone.cpqd-com-br.zone_id}"
  name    = "cpqd-itsm"
  type    = "A"
  records = ["34.228.182.40"]
  ttl = "${var.ttl}"
}

resource "aws_route53_record" "a-cpqd-om-cpqd-com-br" {
  zone_id = "${aws_route53_zone.cpqd-com-br.zone_id}"
  name    = "cpqd-om"
  type    = "A"
  records = ["177.125.143.77"]
  ttl = "${var.ttl}"
}

resource "aws_route53_record" "a-cpqd-pm-cpqd-com-br" {
  zone_id = "${aws_route53_zone.cpqd-com-br.zone_id}"
  name    = "cpqd-pm"
  type    = "A"
  records = ["54.236.65.145"]
  ttl = "${var.ttl}"
}

resource "aws_route53_record" "a-cpqd-com-br" {
  zone_id = "${aws_route53_zone.cpqd-com-br.zone_id}"
  name    = ""
  type    = "A"
  records = ["177.125.142.43","177.125.142.47","177.125.142.49"]
  ttl = "3600"
}

resource "aws_route53_record" "a-cs-admin-cpqd-com-br" {
  zone_id = "${aws_route53_zone.cpqd-com-br.zone_id}"
  name    = "cs-admin"
  type    = "A"
  records = ["34.231.16.192"]
  ttl = "${var.ttl}"
}

resource "aws_route53_record" "a-cs-lb-cpqd-com-br" {
  zone_id = "${aws_route53_zone.cpqd-com-br.zone_id}"
  name    = "cs-lb"
  type    = "A"
  records = ["42.206.146.206","52.87.111.53"]
  ttl = "${var.ttl}"
}

resource "aws_route53_record" "a-cs-cpqd-com-br" {
  zone_id = "${aws_route53_zone.cpqd-com-br.zone_id}"
  name    = "cs"
  type    = "A"
  records = ["177.125.143.77"]
  ttl = "${var.ttl}"
}

resource "aws_route53_record" "a-csreport-cpqd-com-br" {
  zone_id = "${aws_route53_zone.cpqd-com-br.zone_id}"
  name    = "csreport"
  type    = "A"
  records = ["52.67.165.196"]
  ttl = "${var.ttl}"
}

resource "aws_route53_record" "a-csseg-cpqd-com-br" {
  zone_id = "${aws_route53_zone.cpqd-com-br.zone_id}"
  name    = "csseg"
  type    = "A"
  records = ["177.125.142.47"]
  ttl = "${var.ttl}"
}

resource "aws_route53_record" "a-cstransaction-cpqd-com-br" {
  zone_id = "${aws_route53_zone.cpqd-com-br.zone_id}"
  name    = "cstransaction"
  type    = "A"
  records = ["54.94.214.210"]
  ttl = "${var.ttl}"
}

resource "aws_route53_record" "a-decisioncenter-cpqd-com-br" {
  zone_id = "${aws_route53_zone.cpqd-com-br.zone_id}"
  name    = "decisioncenter"
  type    = "A"
  records = ["177.125.143.77"]
  ttl = "${var.ttl}"
}

resource "aws_route53_record" "a-doesangue-cpqd-com-br" {
  zone_id = "${aws_route53_zone.cpqd-com-br.zone_id}"
  name    = "doesangue"
  type    = "A"
  records = ["35.168.161.154"]
  ttl = "${var.ttl}"
}

resource "aws_route53_record" "a-eadclientes-cpqd-com-br" {
  zone_id = "${aws_route53_zone.cpqd-com-br.zone_id}"
  name    = "eadclientes"
  type    = "A"
  records = ["177.125.143.77"]
  ttl = "${var.ttl}"
}

resource "aws_route53_record" "a-estagio-cpqd-com-br" {
  zone_id = "${aws_route53_zone.cpqd-com-br.zone_id}"
  name    = "estagio"
  type    = "A"
  records = ["177.125.143.77"]
  ttl = "${var.ttl}"
}

resource "aws_route53_record" "a-gameofcoin-cpqd-com-br" {
  zone_id = "${aws_route53_zone.cpqd-com-br.zone_id}"
  name    = "gameofcoin"
  type    = "A"
  records = ["177.125.143.77"]
  ttl = "${var.ttl}"
}

resource "aws_route53_record" "a-gea-demo-cpqd-com-br" {
  zone_id = "${aws_route53_zone.cpqd-com-br.zone_id}"
  name    = "gea-demo"
  type    = "A"
  records = ["177.125.143.77"]
  ttl = "${var.ttl}"
}

resource "aws_route53_record" "a-geaenergetica-cpqd-com-br" {
  zone_id = "${aws_route53_zone.cpqd-com-br.zone_id}"
  name    = "geaenergetica"
  type    = "A"
  records = ["177.125.143.77"]
  ttl = "${var.ttl}"
}

resource "aws_route53_record" "a-geagpa-cpqd-com-br" {
  zone_id = "${aws_route53_zone.cpqd-com-br.zone_id}"
  name    = "geagpa"
  type    = "A"
  records = ["177.125.143.77"]
  ttl = "${var.ttl}"
}

resource "aws_route53_record" "a-geatelebras-cpqd-com-br" {
  zone_id = "${aws_route53_zone.cpqd-com-br.zone_id}"
  name    = "geatelebras"
  type    = "A"
  records = ["177.125.143.77"]
  ttl = "${var.ttl}"
}

resource "aws_route53_record" "a-geoanalytics-cpqd-com-br" {
  zone_id = "${aws_route53_zone.cpqd-com-br.zone_id}"
  name    = "geoanalytics"
  type    = "A"
  records = ["177.125.143.77"]
  ttl = "${var.ttl}"
}

resource "aws_route53_record" "a-geocorolla-cpqd-com-br" {
  zone_id = "${aws_route53_zone.cpqd-com-br.zone_id}"
  name    = "geocorolla"
  type    = "A"
  records = ["177.125.143.77"]
  ttl = "${var.ttl}"
}

resource "aws_route53_record" "a-geoviper-cpqd-com-br" {
  zone_id = "${aws_route53_zone.cpqd-com-br.zone_id}"
  name    = "geoviper"
  type    = "A"
  records = ["177.125.143.77"]
  ttl = "${var.ttl}"
}

resource "aws_route53_record" "a-gerrit-cpqd-com-br" {
  zone_id = "${aws_route53_zone.cpqd-com-br.zone_id}"
  name    = "gerrit"
  type    = "A"
  records = ["177.125.143.77"]
  ttl = "${var.ttl}"
}

resource "aws_route53_record" "a-gestaoiluminacaocemig-cpqd-com-br" {
  zone_id = "${aws_route53_zone.cpqd-com-br.zone_id}"
  name    = "gestaoiluminacaocemig"
  type    = "A"
  records = ["177.125.143.77"]
  ttl = "${var.ttl}"
}

resource "aws_route53_record" "a-gpescolas-cpqd-com-br" {
  zone_id = "${aws_route53_zone.cpqd-com-br.zone_id}"
  name    = "gpescolas"
  type    = "A"
  records = ["177.125.143.77"]
  ttl = "${var.ttl}"
}

resource "aws_route53_record" "a-grtit-cpqd-com-br" {
  zone_id = "${aws_route53_zone.cpqd-com-br.zone_id}"
  name    = "grtit"
  type    = "A"
  records = ["177.125.143.77"]
  ttl = "${var.ttl}"
}

resource "aws_route53_record" "a-grtitbba-cpqd-com-br" {
  zone_id = "${aws_route53_zone.cpqd-com-br.zone_id}"
  name    = "grtitbba"
  type    = "A"
  records = ["177.125.143.77"]
  ttl = "${var.ttl}"
}

resource "aws_route53_record" "a-grtithm-cpqd-com-br" {
  zone_id = "${aws_route53_zone.cpqd-com-br.zone_id}"
  name    = "grtithm"
  type    = "A"
  records = ["177.125.143.77"]
  ttl = "${var.ttl}"
}

resource "aws_route53_record" "a-grtitpr-cpqd-com-br" {
  zone_id = "${aws_route53_zone.cpqd-com-br.zone_id}"
  name    = "grtitpr"
  type    = "A"
  records = ["177.125.143.77"]
  ttl = "${var.ttl}"
}

resource "aws_route53_record" "a-grtittr-cpqd-com-br" {
  zone_id = "${aws_route53_zone.cpqd-com-br.zone_id}"
  name    = "grtittr"
  type    = "A"
  records = ["177.125.143.77"]
  ttl = "${var.ttl}"
}

resource "aws_route53_record" "a-grtneo-cpqd-com-br" {
  zone_id = "${aws_route53_zone.cpqd-com-br.zone_id}"
  name    = "grtneo"
  type    = "A"
  records = ["177.125.143.77"]
  ttl = "${var.ttl}"
}

resource "aws_route53_record" "a-grtneohm-cpqd-com-br" {
  zone_id = "${aws_route53_zone.cpqd-com-br.zone_id}"
  name    = "grtneohm"
  type    = "A"
  records = ["177.125.143.77"]
  ttl = "${var.ttl}"
}

resource "aws_route53_record" "a-grtrede-cpqd-com-br" {
  zone_id = "${aws_route53_zone.cpqd-com-br.zone_id}"
  name    = "grtrede"
  type    = "A"
  records = ["177.125.143.77"]
  ttl = "${var.ttl}"
}

resource "aws_route53_record" "a-grtvale-cpqd-com-br" {
  zone_id = "${aws_route53_zone.cpqd-com-br.zone_id}"
  name    = "grtvale"
  type    = "A"
  records = ["177.125.143.77"]
  ttl = "${var.ttl}"
}

resource "aws_route53_record" "a-gs-demo-cpqd-com-br" {
  zone_id = "${aws_route53_zone.cpqd-com-br.zone_id}"
  name    = "gs-demo"
  type    = "A"
  records = ["177.125.143.77"]
  ttl = "${var.ttl}"
}

resource "aws_route53_record" "a-gsenergetica-cpqd-com-br" {
  zone_id = "${aws_route53_zone.cpqd-com-br.zone_id}"
  name    = "gsenergetica"
  type    = "A"
  records = ["177.125.143.77"]
  ttl = "${var.ttl}"
}

resource "aws_route53_record" "a-gsgpa-cpqd-com-br" {
  zone_id = "${aws_route53_zone.cpqd-com-br.zone_id}"
  name    = "gsgpa"
  type    = "A"
  records = ["177.125.143.77"]
  ttl = "${var.ttl}"
}

resource "aws_route53_record" "a-gsnextel-cpqd-com-br" {
  zone_id = "${aws_route53_zone.cpqd-com-br.zone_id}"
  name    = "gsnextel"
  type    = "A"
  records = ["177.125.143.77"]
  ttl = "${var.ttl}"
}

resource "aws_route53_record" "a-gstelebras-cpqd-com-br" {
  zone_id = "${aws_route53_zone.cpqd-com-br.zone_id}"
  name    = "gstelebras"
  type    = "A"
  records = ["177.125.143.77"]
  ttl = "${var.ttl}"
}

resource "aws_route53_record" "a-horas-cpqd-com-br" {
  zone_id = "${aws_route53_zone.cpqd-com-br.zone_id}"
  name    = "horas"
  type    = "A"
  records = ["177.125.143.77"]
  ttl = "${var.ttl}"
}

resource "aws_route53_record" "a-hotspot-cpqd-com-br" {
  zone_id = "${aws_route53_zone.cpqd-com-br.zone_id}"
  name    = "hotspot"
  type    = "A"
  records = ["1.1.1.1"]
  ttl = "${var.ttl}"
}

resource "aws_route53_record" "a-intranet-cpqd-com-br" {
  zone_id = "${aws_route53_zone.cpqd-com-br.zone_id}"
  name    = "intranet"
  type    = "A"
  records = ["177.125.143.77"]
  ttl = "${var.ttl}"
}

resource "aws_route53_record" "a-iotmid-docker-cpqd-com-br" {
  zone_id = "${aws_route53_zone.cpqd-com-br.zone_id}"
  name    = "iotmid-docker"
  type    = "A"
  records = ["177.125.143.72"]
  ttl = "${var.ttl}"
}

resource "aws_route53_record" "a-iotmid-docker42-cpqd-com-br" {
  zone_id = "${aws_route53_zone.cpqd-com-br.zone_id}"
  name    = "iotmid-docker42"
  type    = "A"
  records = ["177.125.143.81"]
  ttl = "${var.ttl}"
}

resource "aws_route53_record" "a-iotmid-docker49-cpqd-com-br" {
  zone_id = "${aws_route53_zone.cpqd-com-br.zone_id}"
  name    = "iotmid-docker49"
  type    = "A"
  records = ["177.125.143.86"]
  ttl = "${var.ttl}"
}

resource "aws_route53_record" "a-iotmid-docker50-cpqd-com-br" {
  zone_id = "${aws_route53_zone.cpqd-com-br.zone_id}"
  name    = "iotmid-docker50"
  type    = "A"
  records = ["177.125.143.87"]
  ttl = "${var.ttl}"
}

resource "aws_route53_record" "a-iotmid01-cpqd-com-br" {
  zone_id = "${aws_route53_zone.cpqd-com-br.zone_id}"
  name    = "iotmid01"
  type    = "A"
  records = ["177.125.143.93"]
  ttl = "${var.ttl}"
}

resource "aws_route53_record" "a-iotmid02-cpqd-com-br" {
  zone_id = "${aws_route53_zone.cpqd-com-br.zone_id}"
  name    = "iotmid02"
  type    = "A"
  records = ["177.125.143.94"]
  ttl = "${var.ttl}"
}

resource "aws_route53_record" "a-iotmid03-cpqd-com-br" {
  zone_id = "${aws_route53_zone.cpqd-com-br.zone_id}"
  name    = "iotmid03"
  type    = "A"
  records = ["177.125.143.85"]
  ttl = "${var.ttl}"
}

resource "aws_route53_record" "a-iotmid04-cpqd-com-br" {
  zone_id = "${aws_route53_zone.cpqd-com-br.zone_id}"
  name    = "iotmid04"
  type    = "A"
  records = ["177.125.143.39"]
  ttl = "${var.ttl}"
}

resource "aws_route53_record" "a-iotmid05-cpqd-com-br" {
  zone_id = "${aws_route53_zone.cpqd-com-br.zone_id}"
  name    = "iotmid05"
  type    = "A"
  records = ["177.125.143.40"]
  ttl = "${var.ttl}"
}

resource "aws_route53_record" "a-iotmid06-cpqd-com-br" {
  zone_id = "${aws_route53_zone.cpqd-com-br.zone_id}"
  name    = "iotmid06"
  type    = "A"
  records = ["177.125.143.44"]
  ttl = "${var.ttl}"
}

resource "aws_route53_record" "a-iotmid07-cpqd-com-br" {
  zone_id = "${aws_route53_zone.cpqd-com-br.zone_id}"
  name    = "iotmid07"
  type    = "A"
  records = ["177.125.143.45"]
  ttl = "${var.ttl}"
}

resource "aws_route53_record" "a-iotmid08-cpqd-com-br" {
  zone_id = "${aws_route53_zone.cpqd-com-br.zone_id}"
  name    = "iotmid08"
  type    = "A"
  records = ["177.125.143.46"]
  ttl = "${var.ttl}"
}

resource "aws_route53_record" "a-iotmid09-cpqd-com-br" {
  zone_id = "${aws_route53_zone.cpqd-com-br.zone_id}"
  name    = "iotmid09"
  type    = "A"
  records = ["177.125.143.90"]
  ttl = "${var.ttl}"
}

resource "aws_route53_record" "a-iotmid10-cpqd-com-br" {
  zone_id = "${aws_route53_zone.cpqd-com-br.zone_id}"
  name    = "iotmid10"
  type    = "A"
  records = ["177.125.143.91"]
  ttl = "${var.ttl}"
}

resource "aws_route53_record" "a-issues-cpqd-com-br" {
  zone_id = "${aws_route53_zone.cpqd-com-br.zone_id}"
  name    = "issues"
  type    = "A"
  records = ["177.125.143.77"]
  ttl = "${var.ttl}"
}

resource "aws_route53_record" "a-jira-cpqd-com-br" {
  zone_id = "${aws_route53_zone.cpqd-com-br.zone_id}"
  name    = "jira"
  type    = "A"
  records = ["177.125.143.77"]
  ttl = "${var.ttl}"
}

resource "aws_route53_record" "a-joinups-cpqd-com-br" {
  zone_id = "${aws_route53_zone.cpqd-com-br.zone_id}"
  name    = "joinups"
  type    = "A"
  records = ["177.125.143.77"]
  ttl = "${var.ttl}"
}

resource "aws_route53_record" "a-license38-cpqd-com-br" {
  zone_id = "${aws_route53_zone.cpqd-com-br.zone_id}"
  name    = "license38"
  type    = "A"
  records = ["52.206.51.232"]
  ttl = "${var.ttl}"
}

resource "aws_route53_record" "a-licenseh-cpqd-com-br" {
  zone_id = "${aws_route53_zone.cpqd-com-br.zone_id}"
  name    = "licenseh"
  type    = "A"
  records = ["177.125.143.77"]
  ttl = "${var.ttl}"
}

resource "aws_route53_record" "a-liveness-cpqd-com-br" {
  zone_id = "${aws_route53_zone.cpqd-com-br.zone_id}"
  name    = "liveness"
  type    = "A"
  records = ["177.125.143.77"]
  ttl = "${var.ttl}"
}

resource "aws_route53_record" "a-maven-cpqd-com-br" {
  zone_id = "${aws_route53_zone.cpqd-com-br.zone_id}"
  name    = "maven"
  type    = "A"
  records = ["34.206.109.239"]
  ttl = "${var.ttl}"
}

resource "aws_route53_record" "a-mbframework-cpqd-com-br" {
  zone_id = "${aws_route53_zone.cpqd-com-br.zone_id}"
  name    = "mbframework"
  type    = "A"
  records = ["177.125.143.77"]
  ttl = "${var.ttl}"
}

resource "aws_route53_record" "a-mobileframework-cpqd-com-br" {
  zone_id = "${aws_route53_zone.cpqd-com-br.zone_id}"
  name    = "mobileframework"
  type    = "A"
  records = ["177.125.143.57"]
  ttl = "${var.ttl}"
}

resource "aws_route53_record" "a-mobileseguro-cpqd-com-br" {
  zone_id = "${aws_route53_zone.cpqd-com-br.zone_id}"
  name    = "mobileseguro"
  type    = "A"
  records = ["54.243.204.160"]
  ttl = "${var.ttl}"
}

resource "aws_route53_record" "a-nda-cpqd-com-br" {
  zone_id = "${aws_route53_zone.cpqd-com-br.zone_id}"
  name    = "nda"
  type    = "A"
  records = ["177.125.143.77"]
  ttl = "${var.ttl}"
}

resource "aws_route53_record" "a-nextel-ocr-cpqd-com-br" {
  zone_id = "${aws_route53_zone.cpqd-com-br.zone_id}"
  name    = "nextel-ocr"
  type    = "A"
  records = ["177.125.143.77"]
  ttl = "${var.ttl}"
}

resource "aws_route53_record" "a-nocdrc-cpqd-com-br" {
  zone_id = "${aws_route53_zone.cpqd-com-br.zone_id}"
  name    = "nocdrc"
  type    = "A"
  records = ["177.125.143.77"]
  ttl = "${var.ttl}"
}

resource "aws_route53_record" "a-nps-cpqd-com-br" {
  zone_id = "${aws_route53_zone.cpqd-com-br.zone_id}"
  name    = "nps"
  type    = "A"
  records = ["177.125.143.77"]
  ttl = "${var.ttl}"
}

resource "aws_route53_record" "a-ossgp-cpqd-com-br" {
  zone_id = "${aws_route53_zone.cpqd-com-br.zone_id}"
  name    = "ossgp"
  type    = "A"
  records = ["177.125.143.77"]
  ttl = "${var.ttl}"
}

resource "aws_route53_record" "a-ossgph2-cpqd-com-br" {
  zone_id = "${aws_route53_zone.cpqd-com-br.zone_id}"
  name    = "ossgph2"
  type    = "A"
  records = ["177.125.143.77"]
  ttl = "${var.ttl}"
}

resource "aws_route53_record" "a-ossgprp-cpqd-com-br" {
  zone_id = "${aws_route53_zone.cpqd-com-br.zone_id}"
  name    = "ossgprp"
  type    = "A"
  records = ["177.125.143.77"]
  ttl = "${var.ttl}"
}

resource "aws_route53_record" "a-pcnsync-cpqd-com-br" {
  zone_id = "${aws_route53_zone.cpqd-com-br.zone_id}"
  name    = "pcnsync"
  type    = "A"
  records = ["54.165.227.199"]
  ttl = "${var.ttl}"
}

resource "aws_route53_record" "a-pgfacebook-cpqd-com-br" {
  zone_id = "${aws_route53_zone.cpqd-com-br.zone_id}"
  name    = "pgfacebook"
  type    = "A"
  records = ["177.125.143.77"]
  ttl = "${var.ttl}"
}

resource "aws_route53_record" "a-pgrest-cpqd-com-br" {
  zone_id = "${aws_route53_zone.cpqd-com-br.zone_id}"
  name    = "pgrest"
  type    = "A"
  records = ["177.125.143.77"]
  ttl = "${var.ttl}"
}

resource "aws_route53_record" "a-pmga-cpqd-com-br" {
  zone_id = "${aws_route53_zone.cpqd-com-br.zone_id}"
  name    = "pmga"
  type    = "A"
  records = ["177.125.143.77"]
  ttl = "${var.ttl}"
}

resource "aws_route53_record" "a-pocsa-cpqd-com-br" {
  zone_id = "${aws_route53_zone.cpqd-com-br.zone_id}"
  name    = "pocsa"
  type    = "A"
  records = ["177.125.143.77"]
  ttl = "${var.ttl}"
}

resource "aws_route53_record" "a-portalwholesale-cpqd-com-br" {
  zone_id = "${aws_route53_zone.cpqd-com-br.zone_id}"
  name    = "portalwholesale"
  type    = "A"
  records = ["177.125.143.77"]
  ttl = "${var.ttl}"
}

resource "aws_route53_record" "a-portalwholesaletim-cpqd-com-br" {
  zone_id = "${aws_route53_zone.cpqd-com-br.zone_id}"
  name    = "portalwholesaletim"
  type    = "A"
  records = ["177.125.143.77"]
  ttl = "${var.ttl}"
}

resource "aws_route53_record" "a-processos-cpqd-com-br" {
  zone_id = "${aws_route53_zone.cpqd-com-br.zone_id}"
  name    = "processos"
  type    = "A"
  records = ["177.125.143.77"]
  ttl = "${var.ttl}"
}

resource "aws_route53_record" "a-res-cpqd-com-br" {
  zone_id = "${aws_route53_zone.cpqd-com-br.zone_id}"
  name    = "res"
  type    = "A"
  records = ["177.125.143.77"]
  ttl = "${var.ttl}"
}

resource "aws_route53_record" "a-rh-cpqd-com-br" {
  zone_id = "${aws_route53_zone.cpqd-com-br.zone_id}"
  name    = "rh"
  type    = "A"
  records = ["177.125.143.77"]
  ttl = "${var.ttl}"
}

resource "aws_route53_record" "a-sa-cpqd-com-br" {
  zone_id = "${aws_route53_zone.cpqd-com-br.zone_id}"
  name    = "sa"
  type    = "A"
  records = ["177.125.143.77"]
  ttl = "${var.ttl}"
}

resource "aws_route53_record" "a-sige-cpqd-com-br" {
  zone_id = "${aws_route53_zone.cpqd-com-br.zone_id}"
  name    = "sige"
  type    = "A"
  records = ["177.125.143.77"]
  ttl = "${var.ttl}"
}

resource "aws_route53_record" "a-smart-cpqd-com-br" {
  zone_id = "${aws_route53_zone.cpqd-com-br.zone_id}"
  name    = "smart"
  type    = "A"
  records = ["177.125.143.77"]
  ttl = "${var.ttl}"
}

resource "aws_route53_record" "a-smartboleto-cpqd-com-br" {
  zone_id = "${aws_route53_zone.cpqd-com-br.zone_id}"
  name    = "smartboleto"
  type    = "A"
  records = ["177.125.143.77"]
  ttl = "${var.ttl}"
}

resource "aws_route53_record" "a-smtp-jira-cpqd-com-br" {
  zone_id = "${aws_route53_zone.cpqd-com-br.zone_id}"
  name    = "smtp-jira"
  type    = "A"
  records = ["52.72.121.91"]
  ttl = "${var.ttl}"
}

resource "aws_route53_record" "a-speech-doc-cpqd-com-br" {
  zone_id = "${aws_route53_zone.cpqd-com-br.zone_id}"
  name    = "speech-doc"
  type    = "A"
  records = ["177.125.143.77"]
  ttl = "${var.ttl}"
}

resource "aws_route53_record" "a-speech-cpqd-com-br" {
  zone_id = "${aws_route53_zone.cpqd-com-br.zone_id}"
  name    = "speech"
  type    = "A"
  records = ["177.125.143.77"]
  ttl = "${var.ttl}"
}

resource "aws_route53_record" "a-speech108-cpqd-com-br" {
  zone_id = "${aws_route53_zone.cpqd-com-br.zone_id}"
  name    = "speech108"
  type    = "A"
  records = ["177.125.143.77"]
  ttl = "${var.ttl}"
}

resource "aws_route53_record" "a-speech2-cpqd-com-br" {
  zone_id = "${aws_route53_zone.cpqd-com-br.zone_id}"
  name    = "speech2"
  type    = "A"
  records = ["52.86.44.168"]
  ttl = "${var.ttl}"
}

resource "aws_route53_record" "a-speechsrv-cpqd-com-br" {
  zone_id = "${aws_route53_zone.cpqd-com-br.zone_id}"
  name    = "speechsrv"
  type    = "A"
  records = ["177.125.143.77"]
  ttl = "${var.ttl}"
}

resource "aws_route53_record" "a-speechweb-cpqd-com-br" {
  zone_id = "${aws_route53_zone.cpqd-com-br.zone_id}"
  name    = "speechweb"
  type    = "A"
  records = ["177.125.143.77"]
  ttl = "${var.ttl}"
}

resource "aws_route53_record" "a-ssm01-cpqd-com-br" {
  zone_id = "${aws_route53_zone.cpqd-com-br.zone_id}"
  name    = "ssm01"
  type    = "A"
  records = ["177.125.143.88"]
  ttl = "${var.ttl}"
}

resource "aws_route53_record" "a-suporte-cpqd-com-br" {
  zone_id = "${aws_route53_zone.cpqd-com-br.zone_id}"
  name    = "suporte"
  type    = "A"
  records = ["177.125.143.77"]
  ttl = "${var.ttl}"
}

resource "aws_route53_record" "a-support-cpqd-com-br" {
  zone_id = "${aws_route53_zone.cpqd-com-br.zone_id}"
  name    = "support"
  type    = "A"
  records = ["177.125.143.77"]
  ttl = "${var.ttl}"
}

resource "aws_route53_record" "a-svn-cpqd-com-br" {
  zone_id = "${aws_route53_zone.cpqd-com-br.zone_id}"
  name    = "svn"
  type    = "A"
  records = ["52.71.207.14"]
  ttl = "${var.ttl}"
}

resource "aws_route53_record" "a-tableaudrc-cpqd-com-br" {
  zone_id = "${aws_route53_zone.cpqd-com-br.zone_id}"
  name    = "tableaudrc"
  type    = "A"
  records = ["177.125.143.77"]
  ttl = "${var.ttl}"
}

resource "aws_route53_record" "a-temliqhm-cpqd-com-br" {
  zone_id = "${aws_route53_zone.cpqd-com-br.zone_id}"
  name    = "temliqhm"
  type    = "A"
  records = ["177.125.143.77"]
  ttl = "${var.ttl}"
}

resource "aws_route53_record" "a-testedog-cpqd-com-br" {
  zone_id = "${aws_route53_zone.cpqd-com-br.zone_id}"
  name    = "testedog"
  type    = "A"
  records = ["177.125.143.77"]
  ttl = "${var.ttl}"
}

resource "aws_route53_record" "a-universo-cpqd-com-br" {
  zone_id = "${aws_route53_zone.cpqd-com-br.zone_id}"
  name    = "universo"
  type    = "A"
  records = ["177.125.143.77"]
  ttl = "${var.ttl}"
}

resource "aws_route53_record" "a-vaas-cpqd-com-br" {
  zone_id = "${aws_route53_zone.cpqd-com-br.zone_id}"
  name    = "vaas"
  type    = "A"
  records = ["177.125.143.77"]
  ttl = "${var.ttl}"
}

resource "aws_route53_record" "a-vaas108-cpqd-com-br" {
  zone_id = "${aws_route53_zone.cpqd-com-br.zone_id}"
  name    = "vaas108"
  type    = "A"
  records = ["177.125.143.77"]
  ttl = "${var.ttl}"
}

resource "aws_route53_record" "a-vaas34-cpqd-com-br" {
  zone_id = "${aws_route53_zone.cpqd-com-br.zone_id}"
  name    = "vaas34"
  type    = "A"
  records = ["177.125.143.77"]
  ttl = "${var.ttl}"
}

resource "aws_route53_record" "a-vaas68-cpqd-com-br" {
  zone_id = "${aws_route53_zone.cpqd-com-br.zone_id}"
  name    = "vaas68"
  type    = "A"
  records = ["177.125.143.77"]
  ttl = "${var.ttl}"
}

resource "aws_route53_record" "a-viagens-cpqd-com-br" {
  zone_id = "${aws_route53_zone.cpqd-com-br.zone_id}"
  name    = "viagens"
  type    = "A"
  records = ["177.125.143.77"]
  ttl = "${var.ttl}"
}

resource "aws_route53_record" "a-vivoweb-cpqd-com-br" {
  zone_id = "${aws_route53_zone.cpqd-com-br.zone_id}"
  name    = "vivoweb"
  type    = "A"
  records = ["177.125.143.77"]
  ttl = "${var.ttl}"
}

resource "aws_route53_record" "a-vmescorpiao-cpqd-com-br" {
  zone_id = "${aws_route53_zone.cpqd-com-br.zone_id}"
  name    = "vmescorpiao"
  type    = "A"
  records = ["177.125.142.69"]
  ttl = "${var.ttl}"
}

resource "aws_route53_record" "a-vmjade-cpqd-com-br" {
  zone_id = "${aws_route53_zone.cpqd-com-br.zone_id}"
  name    = "vmjade"
  type    = "A"
  records = ["177.125.143.88"]
  ttl = "${var.ttl}"
}

resource "aws_route53_record" "a-vmmobile-cpqd-com-br" {
  zone_id = "${aws_route53_zone.cpqd-com-br.zone_id}"
  name    = "vmmobile"
  type    = "A"
  records = ["177.125.143.157"]
  ttl = "${var.ttl}"
}

resource "aws_route53_record" "a-vocalbanking-cpqd-com-br" {
  zone_id = "${aws_route53_zone.cpqd-com-br.zone_id}"
  name    = "vocalbanking"
  type    = "A"
  records = ["177.125.143.77"]
  ttl = "${var.ttl}"
}

resource "aws_route53_record" "a-webappselfie-cpqd-com-br" {
  zone_id = "${aws_route53_zone.cpqd-com-br.zone_id}"
  name    = "webappselfie"
  type    = "A"
  records = ["54.175.26.187"]
  ttl = "${var.ttl}"
}

resource "aws_route53_record" "a-www-cpqd-com-br" {
  zone_id = "${aws_route53_zone.cpqd-com-br.zone_id}"
  name    = "www"
  type    = "A"
  records = ["177.125.142.43","177.125.142.47","177.125.142.49"]
  ttl = "${var.ttl}"
}

resource "aws_route53_record" "a-zai-sfc-retail-cpqd-com-br" {
  zone_id = "${aws_route53_zone.cpqd-com-br.zone_id}"
  name    = "zai-sfc-retail"
  type    = "A"
  records = ["177.125.143.77"]
  ttl = "${var.ttl}"
}

resource "aws_route53_record" "a-zurich-cpqd-com-br" {
  zone_id = "${aws_route53_zone.cpqd-com-br.zone_id}"
  name    = "zurich-cp"
  type    = "A"
  records = ["177.125.143.99"]
  ttl = "${var.ttl}"
}

resource "aws_route53_record" "aaaa-barao-cpqd-com-br" {
  zone_id = "${aws_route53_zone.cpqd-com-br.zone_id}"
  name    = "barao"
  type    = "AAAA"
  records = ["2801:b8:101:d01:0:0:0:10"]
  ttl = "${var.ttl}"
}

resource "aws_route53_record" "aaaa-conde-cpqd-com-br" {
  zone_id = "${aws_route53_zone.cpqd-com-br.zone_id}"
  name    = "conde"
  type    = "AAAA"
  records = ["2801:b8:101:d01:0:0:0:11"]
  ttl = "${var.ttl}"
}

resource "aws_route53_record" "aaaa-duque-cpqd-com-br" {
  zone_id = "${aws_route53_zone.cpqd-com-br.zone_id}"
  name    = "duque"
  type    = "AAAA"
  records = ["2801:b8:101:d01:0:0:0:12"]
  ttl = "${var.ttl}"
}

resource "aws_route53_record" "aaaa-www-cpqd-com-br" {
  zone_id = "${aws_route53_zone.cpqd-com-br.zone_id}"
  name    = "www"
  type    = "AAAA"
  records = ["2801:b8:101:d01:0:0:0:10","2801:b8:101:d01:0:0:0:11","2801:b8:101:d01:0:0:0:12"]
  ttl = "${var.ttl}"
}

resource "aws_route53_record" "cname-1547277-cpqd-com-br" {
  zone_id = "${aws_route53_zone.cpqd-com-br.zone_id}"
  name    = "1547277"
  type    = "CNAME"
  records = ["sendgrid.net."]
  ttl = "${var.ttl}"
}

resource "aws_route53_record" "cname-agenda-cpqd-com-br" {
  zone_id = "${aws_route53_zone.cpqd-com-br.zone_id}"
  name    = "agenda"
  type    = "CNAME"
  records = ["ghs.google.com."]
  ttl = "${var.ttl}"
}

resource "aws_route53_record" "cname-alb-speech108-cpqd-com-br" {
  zone_id = "${aws_route53_zone.cpqd-com-br.zone_id}"
  name    = "alb-speech108"
  type    = "CNAME"
  records = ["alb-speech108-1449904477.us-east-1.elb.amazonaws.com."]
  ttl = "3600"
}

resource "aws_route53_record" "cname-alfresco-cpqd-com-br" {
  zone_id = "${aws_route53_zone.cpqd-com-br.zone_id}"
  name    = "alfresco"
  type    = "CNAME"
  records = ["svn.cpqd.com.br."]
  ttl = "${var.ttl}"
}

resource "aws_route53_record" "cname-api-cpqd-com-br" {
  zone_id = "${aws_route53_zone.cpqd-com-br.zone_id}"
  name    = "api"
  type    = "CNAME"
  records = ["www.cpqd.com.br."]
  ttl = "${var.ttl}"
}

resource "aws_route53_record" "cname-apps-cpqd-com-br" {
  zone_id = "${aws_route53_zone.cpqd-com-br.zone_id}"
  name    = "apps"
  type    = "CNAME"
  records = ["ghs.google.com."]
  ttl = "${var.ttl}"
}

resource "aws_route53_record" "cname-asrblx-cpqd-com-br" {
  zone_id = "${aws_route53_zone.cpqd-com-br.zone_id}"
  name    = "asrblx"
  type    = "CNAME"
  records = ["lbasr8k-1379291-dal13.lb.bluemix.net."]
  ttl = "${var.ttl}"
}

resource "aws_route53_record" "cname-avaliacao-cpqd-com-br" {
  zone_id = "${aws_route53_zone.cpqd-com-br.zone_id}"
  name    = "avaliacao"
  type    = "CNAME"
  records = ["www.cpqd.com.br."]
  ttl = "${var.ttl}"
}

resource "aws_route53_record" "cname-cds-cpqd-com-br" {
  zone_id = "${aws_route53_zone.cpqd-com-br.zone_id}"
  name    = "cds"
  type    = "CNAME"
  records = ["ghs.google.com."]
  ttl = "${var.ttl}"
}

resource "aws_route53_record" "cname-comunidade-cpqd-com-br" {
  zone_id = "${aws_route53_zone.cpqd-com-br.zone_id}"
  name    = "comunidade"
  type    = "CNAME"
  records = ["www.cpqd.com.br."]
  ttl = "${var.ttl}"
}

resource "aws_route53_record" "cname-cpqdh-cpqd-com-br" {
  zone_id = "${aws_route53_zone.cpqd-com-br.zone_id}"
  name    = "cpqdh"
  type    = "CNAME"
  records = ["www.cpqd.com.br."]
  ttl = "${var.ttl}"
}

resource "aws_route53_record" "cname-cpqdweb-cpqd-com-br" {
  zone_id = "${aws_route53_zone.cpqd-com-br.zone_id}"
  name    = "cpqdweb"
  type    = "CNAME"
  records = ["www.cpqd.com.br."]
  ttl = "${var.ttl}"
}

resource "aws_route53_record" "cname-de51-cpqd-com-br" {
  zone_id = "${aws_route53_zone.cpqd-com-br.zone_id}"
  name    = "de51"
  type    = "CNAME"
  records = ["www.cpqd.com.br."]
  ttl = "${var.ttl}"
}

resource "aws_route53_record" "cname-desafio-dojot-iot-cpqd-com-br" {
  zone_id = "${aws_route53_zone.cpqd-com-br.zone_id}"
  name    = "desafio-dojot-iot"
  type    = "CNAME"
  records = ["desafio-dojot-801b439b73aaf0f2.elb.us-east-1.amazonaws.com."]
  ttl = "3600"
}

resource "aws_route53_record" "cname-desafio-dojot-cpqd-com-br" {
  zone_id = "${aws_route53_zone.cpqd-com-br.zone_id}"
  name    = "desafio-dojot"
  type    = "CNAME"
  records = ["www-desafio-dojot-569364686.us-east-1.elb.amazonaws.com."]
  ttl = "3600"
}

resource "aws_route53_record" "cname-docs-cpqd-com-br" {
  zone_id = "${aws_route53_zone.cpqd-com-br.zone_id}"
  name    = "docs"
  type    = "CNAME"
  records = ["ghs.google.com."]
  ttl = "${var.ttl}"
}

resource "aws_route53_record" "cname-drc-cpqd-com-br" {
  zone_id = "${aws_route53_zone.cpqd-com-br.zone_id}"
  name    = "drc"
  type    = "CNAME"
  records = ["ghs.google.com."]
  ttl = "${var.ttl}"
}

resource "aws_route53_record" "cname-ead-cpqd-com-br" {
  zone_id = "${aws_route53_zone.cpqd-com-br.zone_id}"
  name    = "ead"
  type    = "CNAME"
  records = ["www.cpqd.com.br."]
  ttl = "${var.ttl}"
}

resource "aws_route53_record" "cname-em4463-cpqd-com-br" {
  zone_id = "${aws_route53_zone.cpqd-com-br.zone_id}"
  name    = "em4463"
  type    = "CNAME"
  records = ["u7472020.wl021.sendgrid.net."]
  ttl = "3600"
}

resource "aws_route53_record" "cname-fisheye-cpqd-com-br" {
  zone_id = "${aws_route53_zone.cpqd-com-br.zone_id}"
  name    = "fisheye"
  type    = "CNAME"
  records = ["svn.cpqd.com.br."]
  ttl = "${var.ttl}"
}

resource "aws_route53_record" "cname-gea-cpqd-com-br" {
  zone_id = "${aws_route53_zone.cpqd-com-br.zone_id}"
  name    = "gea"
  type    = "CNAME"
  records = ["www.cpqd.com.br."]
  ttl = "${var.ttl}"
}

resource "aws_route53_record" "cname-go-cpqd-com-br" {
  zone_id = "${aws_route53_zone.cpqd-com-br.zone_id}"
  name    = "go"
  type    = "CNAME"
  records = ["ghs.google.com."]
  ttl = "${var.ttl}"
}

resource "aws_route53_record" "cname-googleab5a064b33925236-cpqd-com-br" {
  zone_id = "${aws_route53_zone.cpqd-com-br.zone_id}"
  name    = "googleab5a064b33925236"
  type    = "CNAME"
  records = ["google.com."]
  ttl = "${var.ttl}"
}

resource "aws_route53_record" "cname-grupos-cpqd-com-br" {
  zone_id = "${aws_route53_zone.cpqd-com-br.zone_id}"
  name    = "grupos"
  type    = "CNAME"
  records = ["ghs.google.com."]
  ttl = "${var.ttl}"
}

resource "aws_route53_record" "cname-gti-cpqd-com-br" {
  zone_id = "${aws_route53_zone.cpqd-com-br.zone_id}"
  name    = "gti"
  type    = "CNAME"
  records = ["ghs.google.com."]
  ttl = "${var.ttl}"
}

resource "aws_route53_record" "cname-helpdesk-cpqd-com-br" {
  zone_id = "${aws_route53_zone.cpqd-com-br.zone_id}"
  name    = "helpdesk"
  type    = "CNAME"
  records = ["jira.cpqd.com.br."]
  ttl = "${var.ttl}"
}

resource "aws_route53_record" "cname-jenkins-cpqd-com-br" {
  zone_id = "${aws_route53_zone.cpqd-com-br.zone_id}"
  name    = "jenkins"
  type    = "CNAME"
  records = ["www.cpqd.com.br."]
  ttl = "${var.ttl}"
}

resource "aws_route53_record" "cname-login-cpqd-com-br" {
  zone_id = "${aws_route53_zone.cpqd-com-br.zone_id}"
  name    = "login"
  type    = "CNAME"
  records = ["www.cpqd.com.br."]
  ttl = "${var.ttl}"
}

resource "aws_route53_record" "cname-m-cpqd-com-br" {
  zone_id = "${aws_route53_zone.cpqd-com-br.zone_id}"
  name    = "m"
  type    = "CNAME"
  records = ["www.cpqd.com.br."]
  ttl = "${var.ttl}"
}

resource "aws_route53_record" "cname-mail-cpqd-com-br" {
  zone_id = "${aws_route53_zone.cpqd-com-br.zone_id}"
  name    = "mail"
  type    = "CNAME"
  records = ["ghs.google.com."]
  ttl = "${var.ttl}"
}

resource "aws_route53_record" "cname-materiais-cpqd-com-br" {
  zone_id = "${aws_route53_zone.cpqd-com-br.zone_id}"
  name    = "materiais"
  type    = "CNAME"
  records = ["pages.rdstation.com.br."]
  ttl = "${var.ttl}"
}

resource "aws_route53_record" "cname-mx-cpqd-com-br" {
  zone_id = "${aws_route53_zone.cpqd-com-br.zone_id}"
  name    = "mx"
  type    = "CNAME"
  records = ["ASPMX.L.GOOGLE.COM."]
  ttl = "3600"
}

resource "aws_route53_record" "cname-noc-cpqd-com-br" {
  zone_id = "${aws_route53_zone.cpqd-com-br.zone_id}"
  name    = "noc"
  type    = "CNAME"
  records = ["www.cpqd.com.br."]
  ttl = "${var.ttl}"
}

resource "aws_route53_record" "cname-opensso-cpqd-com-br" {
  zone_id = "${aws_route53_zone.cpqd-com-br.zone_id}"
  name    = "opensso"
  type    = "CNAME"
  records = ["www.cpqd.com.br."]
  ttl = "${var.ttl}"
}

resource "aws_route53_record" "cname-portalconselho-cpqd-com-br" {
  zone_id = "${aws_route53_zone.cpqd-com-br.zone_id}"
  name    = "portalconselho"
  type    = "CNAME"
  records = ["svn.cpqd.com.br."]
  ttl = "${var.ttl}"
}

resource "aws_route53_record" "cname-primavera-cpqd-com-br" {
  zone_id = "${aws_route53_zone.cpqd-com-br.zone_id}"
  name    = "primavera"
  type    = "CNAME"
  records = ["www.cpqd.com.br."]
  ttl = "${var.ttl}"
}

resource "aws_route53_record" "cname-rds-_domainkey-cpqd-com-br" {
  zone_id = "${aws_route53_zone.cpqd-com-br.zone_id}"
  name    = "rds._domainkey"
  type    = "CNAME"
  records = ["rds.domainkey.u7472020.wl021.sendgrid.net."]
  ttl = "3600"
}

resource "aws_route53_record" "cname-rds2-_domainkey-cpqd-com-br" {
  zone_id = "${aws_route53_zone.cpqd-com-br.zone_id}"
  name    = "rds2._domainkey"
  type    = "CNAME"
  records = ["rds2.domainkey.u7472020.wl021.sendgrid.net."]
  ttl = "3600"
}

resource "aws_route53_record" "cname-rsvn-cpqd-com-br" {
  zone_id = "${aws_route53_zone.cpqd-com-br.zone_id}"
  name    = "rsvn"
  type    = "CNAME"
  records = ["svn.cpqd.com.br."]
  ttl = "${var.ttl}"
}

resource "aws_route53_record" "cname-s1-_domainkey-cpqd-com-br" {
  zone_id = "${aws_route53_zone.cpqd-com-br.zone_id}"
  name    = "s1._domainkey"
  type    = "CNAME"
  records = ["s1.domainkey.u1547277.wl028.sendgrid.net."]
  ttl = "${var.ttl}"
}

resource "aws_route53_record" "cname-s2-_domainkey-cpqd-com-br" {
  zone_id = "${aws_route53_zone.cpqd-com-br.zone_id}"
  name    = "s2._domainkey"
  type    = "CNAME"
  records = ["s2.domainkey.u1547277.wl028.sendgrid.net."]
  ttl = "${var.ttl}"
}

resource "aws_route53_record" "cname-shared-resources-cpqd-com-br" {
  zone_id = "${aws_route53_zone.cpqd-com-br.zone_id}"
  name    = "shared-resources"
  type    = "CNAME"
  records = ["ghs.googlehosted.com."]
  ttl = "3600"
}

resource "aws_route53_record" "cname-sites-cpqd-com-br" {
  zone_id = "${aws_route53_zone.cpqd-com-br.zone_id}"
  name    = "sites"
  type    = "CNAME"
  records = ["ghs.google.com."]
  ttl = "${var.ttl}"
}

resource "aws_route53_record" "cname-snd-cpqd-com-br" {
  zone_id = "${aws_route53_zone.cpqd-com-br.zone_id}"
  name    = "snd"
  type    = "CNAME"
  records = ["u1547277.wl028.sendgrid.net."]
  ttl = "${var.ttl}"
}

resource "aws_route53_record" "cname-start-cpqd-com-br" {
  zone_id = "${aws_route53_zone.cpqd-com-br.zone_id}"
  name    = "start"
  type    = "CNAME"
  records = ["ghs.google.com."]
  ttl = "${var.ttl}"
}

resource "aws_route53_record" "cname-t-snd-cpqd-com-br" {
  zone_id = "${aws_route53_zone.cpqd-com-br.zone_id}"
  name    = "t.snd"
  type    = "CNAME"
  records = ["sendgrid.net."]
  ttl = "${var.ttl}"
}

resource "aws_route53_record" "cname-testlink-cpqd-com-br" {
  zone_id = "${aws_route53_zone.cpqd-com-br.zone_id}"
  name    = "testlink"
  type    = "CNAME"
  records = ["www.cpqd.com.br."]
  ttl = "${var.ttl}"
}

resource "aws_route53_record" "cname-ti-cpqd-com-br" {
  zone_id = "${aws_route53_zone.cpqd-com-br.zone_id}"
  name    = "ti"
  type    = "CNAME"
  records = ["ghs.google.com."]
  ttl = "${var.ttl}"
}

resource "aws_route53_record" "cname-tsmail-cpqd-com-br" {
  zone_id = "${aws_route53_zone.cpqd-com-br.zone_id}"
  name    = "tsmail"
  type    = "CNAME"
  records = ["u2445086.wl087.sendgrid.net."]
  ttl = "${var.ttl}"
}

resource "aws_route53_record" "cname-tss1-_domainkey-cpqd-com-br" {
  zone_id = "${aws_route53_zone.cpqd-com-br.zone_id}"
  name    = "tss1._domainkey"
  type    = "CNAME"
  records = ["s1.domainkey.u2445086.wl087.sendgrid.net."]
  ttl = "${var.ttl}"
}

resource "aws_route53_record" "cname-tss2-_domainkey-cpqd-com-br" {
  zone_id = "${aws_route53_zone.cpqd-com-br.zone_id}"
  name    = "tss2._domainkey"
  type    = "CNAME"
  records = ["s2.domainkey.u2445086.wl087.sendgrid.net."]
  ttl = "${var.ttl}"
}

resource "aws_route53_record" "cname-video-cpqd-com-br" {
  zone_id = "${aws_route53_zone.cpqd-com-br.zone_id}"
  name    = "video"
  type    = "CNAME"
  records = ["ghs.google.com."]
  ttl = "${var.ttl}"
}

resource "aws_route53_record" "cname-voiceanywhere-cpqd-com-br" {
  zone_id = "${aws_route53_zone.cpqd-com-br.zone_id}"
  name    = "voiceanywhere"
  type    = "CNAME"
  records = ["www.cpqd.com.br."]
  ttl = "${var.ttl}"
}

resource "aws_route53_record" "cname-vpncpqd-cpqd-com-br" {
  zone_id = "${aws_route53_zone.cpqd-com-br.zone_id}"
  name    = "vpncpqd"
  type    = "CNAME"
  records = ["zurich-cp.cpqd.com.br."]
  ttl = "${var.ttl}"
}

resource "aws_route53_record" "cname-zaimobile-cpqd-com-br" {
  zone_id = "${aws_route53_zone.cpqd-com-br.zone_id}"
  name    = "zaimobile"
  type    = "CNAME"
  records = ["www.cpqd.com.br."]
  ttl = "${var.ttl}"
}
