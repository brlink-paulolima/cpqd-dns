resource "aws_route53_zone" "giga-org-br" { name = "giga.org.br" }

resource "aws_route53_record" "a-lista-giga-org-br" {
  zone_id = "${aws_route53_zone.giga-org-br.zone_id}"
  name    = "lista"
  type    = "A"
  records = ["177.125.142.47"]
  ttl = "${var.ttl}"
}


resource "aws_route53_record" "cname-agenda-giga-org-br" {
  zone_id = "${aws_route53_zone.giga-org-br.zone_id}"
  name    = "agenda"
  type    = "CNAME"
  records = ["ghs.google.com."]
  ttl = "${var.ttl}"
}

resource "aws_route53_record" "cname-docs-giga-org-br" {
  zone_id = "${aws_route53_zone.giga-org-br.zone_id}"
  name    = "docs"
  type    = "CNAME"
  records = ["ghs.google.com."]
  ttl = "${var.ttl}"
}

resource "aws_route53_record" "cname-mail-giga-org-br" {
  zone_id = "${aws_route53_zone.giga-org-br.zone_id}"
  name    = "mail"
  type    = "CNAME"
  records = ["ghs.google.com."]
  ttl = "${var.ttl}"
}

resource "aws_route53_record" "cname-sites-giga-org-br" {
  zone_id = "${aws_route53_zone.giga-org-br.zone_id}"
  name    = "sites"
  type    = "CNAME"
  records = ["ghs.google.com."]
  ttl = "${var.ttl}"
}

resource "aws_route53_record" "cname-www-giga-org-br" {
  zone_id = "${aws_route53_zone.giga-org-br.zone_id}"
  name    = "www"
  type    = "CNAME"
  records = ["ghs.google.com."]
  ttl = "${var.ttl}"
}

resource "aws_route53_record" "mx-giga-org-br" {
  zone_id = "${aws_route53_zone.giga-org-br.zone_id}"
  name    = ""
  type    = "MX"
  records = ["10 ASPMX.L.GOOGLE.COM.","20 ALT1.ASPMX.L.GOOGLE.COM.","20 ALT2.ASPMX.L.GOOGLE.COM.","30 ASPMX2.GOOGLEMAIL.COM.","30 ASPMX3.GOOGLEMAIL.COM.","30 ASPMX4.GOOGLEMAIL.COM.","30 ASPMX5.GOOGLEMAIL.COM."]
  ttl = "${var.ttl}"
}

resource "aws_route53_record" "mx-lista-giga-org-br" {
  zone_id = "${aws_route53_zone.giga-org-br.zone_id}"
  name    = "lista"
  type    = "MX"
  records = ["10 lista.giga.org.br."]
  ttl = "${var.ttl}"
}

resource "aws_route53_record" "mx-listas-giga-org-br" {
  zone_id = "${aws_route53_zone.giga-org-br.zone_id}"
  name    = "listas"
  type    = "MX"
  records = ["10 ariel.na-df.rnp.br.","20 arpoador.nc-rj.rnp.br."]
  ttl = "${var.ttl}"
}

resource "aws_route53_record" "txt-giga-org-br" {
  zone_id = "${aws_route53_zone.giga-org-br.zone_id}"
  name    = ""
  type    = "TXT"
  records = ["v=spf1 include:_spf.google.com ~all","google-site-verification=sBFvnrypChVjGt9VA0HirilVEgua-x7AAx-egL2sSiU"]
  ttl = "${var.ttl}"
}
