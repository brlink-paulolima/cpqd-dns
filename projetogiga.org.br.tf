resource "aws_route53_zone" "projetogiga-org-br" { name = "projetogiga.org.br" }

resource "aws_route53_record" "cname-www-projetogiga-org-br" {
  zone_id = "${aws_route53_zone.projetogiga-org-br.zone_id}"
  name    = "www"
  type    = "CNAME"
  records = ["ghs.google.com."]
  ttl = "${var.ttl}"
}

resource "aws_route53_record" "mx-projetogiga-org-br" {
  zone_id = "${aws_route53_zone.projetogiga-org-br.zone_id}"
  name    = ""
  type    = "MX"
  records = ["0 barao.cpqd.com.br."]
  ttl = "${var.ttl}"
}

