resource "aws_route53_zone" "polisdetecnologia-com-br" { name = "polisdetecnologia.com.br" }

resource "aws_route53_record" "a-polisdetecnologia-com-br" {
  zone_id = "${aws_route53_zone.polisdetecnologia-com-br.zone_id}"
  name    = ""
  type    = "A"
  records = ["52.216.165.186"]
  ttl = "${var.ttl}"
}

resource "aws_route53_record" "cname-agenda-polisdetecnologia-com-br" {
  zone_id = "${aws_route53_zone.polisdetecnologia-com-br.zone_id}"
  name    = "agenda"
  type    = "CNAME"
  records = ["ghs.google.com."]
  ttl = "${var.ttl}"
}

resource "aws_route53_record" "cname-docs-polisdetecnologia-com-br" {
  zone_id = "${aws_route53_zone.polisdetecnologia-com-br.zone_id}"
  name    = "docs"
  type    = "CNAME"
  records = ["ghs.google.com."]
  ttl = "${var.ttl}"
}

resource "aws_route53_record" "cname-mail-polisdetecnologia-com-br" {
  zone_id = "${aws_route53_zone.polisdetecnologia-com-br.zone_id}"
  name    = "mail"
  type    = "CNAME"
  records = ["ghs.google.com."]
  ttl = "${var.ttl}"
}

resource "aws_route53_record" "cname-sites-polisdetecnologia-com-br" {
  zone_id = "${aws_route53_zone.polisdetecnologia-com-br.zone_id}"
  name    = "sites"
  type    = "CNAME"
  records = ["ghs.google.com."]
  ttl = "${var.ttl}"
}

resource "aws_route53_record" "cname-www-polisdetecnologia-com-br" {
  zone_id = "${aws_route53_zone.polisdetecnologia-com-br.zone_id}"
  name    = "www"
  type    = "CNAME"
  records = ["polisdetecnologia.s3-website-us-east-1.amazonaws.com."]
  ttl = "${var.ttl}"
}

resource "aws_route53_record" "mx-polisdetecnologia-com-br" {
  zone_id = "${aws_route53_zone.polisdetecnologia-com-br.zone_id}"
  name    = ""
  type    = "MX"
  records = ["1 ASPMX.L.GOOGLE.COM.","5 ALT1.ASPMX.L.GOOGLE.COM.","5 ALT2.ASPMX.L.GOOGLE.COM.","10 ASPMX2.GOOGLEMAIL.COM.","10 ASPMX3.GOOGLEMAIL.COM."]
  ttl = "3600"
}

resource "aws_route53_record" "txt-polisdetecnologia-com-br" {
  zone_id = "${aws_route53_zone.polisdetecnologia-com-br.zone_id}"
  name    = ""
  type    = "TXT"
  records = ["google-site-verification=9XX7pVh_U8p7ki36X7RSp3Q11-FdEpLMhyyC87VhvPk","v=spf1 include:_spf.google.com ~all"]
  ttl = "${var.ttl}"
}
