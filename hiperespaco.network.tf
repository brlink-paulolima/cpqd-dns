resource "aws_route53_zone" "hiperespaco-network" { name = "hiperespaco.network" }

resource "aws_route53_record" "a-hiperespaco-network" {
  zone_id = "${aws_route53_zone.hiperespaco-network.zone_id}"
  name    = ""
  type    = "A"
  records = ["177.125.143.77"]
  ttl = "${var.ttl}"
}

resource "aws_route53_record" "a-www-hiperespaco-network" {
  zone_id = "${aws_route53_zone.hiperespaco-network.zone_id}"
  name    = "www"
  type    = "A"
  records = ["177.125.143.77"]
  ttl = "${var.ttl}"
}

resource "aws_route53_record" "mx-hiperespaco-network" {
  zone_id = "${aws_route53_zone.hiperespaco-network.zone_id}"
  name    = ""
  type    = "MX"
  records = ["10 ASPMX.L.GOOGLE.COM.","20 ALT1.ASPMX.L.GOOGLE.COM.","20 ALT2.ASPMX.L.GOOGLE.COM.","30 ASPMX2.GOOGLEMAIL.COM.","30 ASPMX3.GOOGLEMAIL.COM."]
  ttl = "${var.ttl}"
}

resource "aws_route53_record" "txt-hiperespaco-network" {
  zone_id = "${aws_route53_zone.hiperespaco-network.zone_id}"
  name    = ""
  type    = "TXT"
  records = ["google-site-verification=tWXiea3eFvnbrSGIY9XhPjejWrodVCqEXY6_KR6SWBE"]
  ttl = "${var.ttl}"
}
