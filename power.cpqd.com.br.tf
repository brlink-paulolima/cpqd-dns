resource "aws_route53_zone" "power-cpqd-com-br" { name = "power.cpqd.com.br" }

resource "aws_route53_record" "cname-calendar-power-cpqd-com-br" {
  zone_id = "${aws_route53_zone.power-cpqd-com-br.zone_id}"
  name    = "calendar"
  type    = "CNAME"
  records = ["ghs.google.com."]
  ttl = "${var.ttl}"
}

resource "aws_route53_record" "cname-docs-power-cpqd-com-br" {
  zone_id = "${aws_route53_zone.power-cpqd-com-br.zone_id}"
  name    = "docs"
  type    = "CNAME"
  records = ["ghs.google.com."]
  ttl = "${var.ttl}"
}

resource "aws_route53_record" "cname-googleffffffff9fb51a64-power-cpqd-com-br" {
  zone_id = "${aws_route53_zone.power-cpqd-com-br.zone_id}"
  name    = "googleffffffff9fb51a64"
  type    = "CNAME"
  records = ["google.com."]
  ttl = "${var.ttl}"
}

resource "aws_route53_record" "cname-googleffffffffbeb157b3-power-cpqd-com-br" {
  zone_id = "${aws_route53_zone.power-cpqd-com-br.zone_id}"
  name    = "googleffffffffbeb157b3"
  type    = "CNAME"
  records = ["google.com."]
  ttl = "${var.ttl}"
}

resource "aws_route53_record" "cname-mail-power-cpqd-com-br" {
  zone_id = "${aws_route53_zone.power-cpqd-com-br.zone_id}"
  name    = "mail"
  type    = "CNAME"
  records = ["ghs.google.com."]
  ttl = "${var.ttl}"
}

resource "aws_route53_record" "cname-sites-power-cpqd-com-br" {
  zone_id = "${aws_route53_zone.power-cpqd-com-br.zone_id}"
  name    = "sites"
  type    = "CNAME"
  records = ["ghs.google.com."]
  ttl = "${var.ttl}"
}

resource "aws_route53_record" "mx-power-cpqd-com-br" {
  zone_id = "${aws_route53_zone.power-cpqd-com-br.zone_id}"
  name    = ""
  type    = "MX"
  records = ["20 ALT1.ASPMX.L.GOOGLE.COM.","10 ASPMX.L.GOOGLE.COM.","20 ALT2.ASPMX.L.GOOGLE.COM.","30 ASPMX2.GOOGLEMAIL.COM.","30 ASPMX3.GOOGLEMAIL.COM.","30 ASPMX4.GOOGLEMAIL.COM.","30 ASPMX5.GOOGLEMAIL.COM."]
  ttl = "${var.ttl}"
}

resource "aws_route53_record" "txt-power-cpqd-com-br" {
  zone_id = "${aws_route53_zone.power-cpqd-com-br.zone_id}"
  name    = ""
  type    = "TXT"
  records = ["v=spf1 include:aspmx.googlemail.com ~all"]
  ttl = "${var.ttl}"
}
