resource "aws_route53_zone" "ga-cpqd-com-br" { name = "ga.cpqd.com.br" }

resource "aws_route53_record" "mx-ga-cpqd-com-br" {
  zone_id = "${aws_route53_zone.ga-cpqd-com-br.zone_id}"
  name    = ""
  type    = "MX"
  records = ["10 ASPMX.L.GOOGLE.COM.","20 ALT1.ASPMX.L.GOOGLE.COM.","20 ALT2.ASPMX.L.GOOGLE.COM.","30 ASPMX2.GOOGLEMAIL.COM.","30 ASPMX3.GOOGLEMAIL.COM"]
  ttl = "3600"
}
