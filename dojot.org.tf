resource "aws_route53_zone" "dojot-org" { name = "dojot.org" }

resource "aws_route53_record" "mx-dojot-org" {
  zone_id = "${aws_route53_zone.dojot-org.zone_id}"
  name    = ""
  type    = "MX"
  records = ["10 ASPMX.L.GOOGLE.COM.","20 ALT1.ASPMX.L.GOOGLE.COM.","20 ALT2.ASPMX.L.GOOGLE.COM.","30 ASPMX2.GOOGLEMAIL.COM.","30 ASPMX3.GOOGLEMAIL.COM."]
  ttl = "3600"
}

resource "aws_route53_record" "a-dojot-org" {
  zone_id = "${aws_route53_zone.dojot-org.zone_id}"
  name    = ""
  type    = "A"
  records = ["177.125.143.77"]
  ttl = "3600"
}

resource "aws_route53_record" "a-www-dojot-org" {
  zone_id = "${aws_route53_zone.dojot-org.zone_id}"
  name    = "www"
  type    = "A"
  records = ["177.125.143.77"]
  ttl = "3600"
 }

resource "aws_route53_record" "txt-dojot-org" {
  zone_id = "${aws_route53_zone.dojot-org.zone_id}"
  name    = ""
  type    = "TXT"
  records = ["google-site-verification=FsmyCYMXxhzJRB9VZ2xPHDrzANpsILx45OB-E4o-dy4"]
  ttl = "3600"
  }
