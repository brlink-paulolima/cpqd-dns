resource "aws_route53_zone" "stic-net-br" { name = "stic.net.br" }

resource "aws_route53_record" "cname-www-stic-net-br" {
  zone_id = "${aws_route53_zone.stic-net-br.zone_id}"
  name    = "www"
  type    = "CNAME"
  records = ["www.cpqd.com.br."]
  ttl = "${var.ttl}"
}

resource "aws_route53_record" "mx-stic-net-br" {
  zone_id = "${aws_route53_zone.stic-net-br.zone_id}"
  name    = ""
  type    = "MX"
  records = ["0 barao.cpqd.com.br."]
  ttl = "${var.ttl}"
}

