resource "aws_route53_zone" "cpqd-net" { name = "cpqd.net" }

resource "aws_route53_record" "a-cpqd-net" {
  zone_id = "${aws_route53_zone.cpqd-net.zone_id}"
  name    = "www"
  type    = "A"
  records = ["177.125.142.49","177.125.142.47","177.125.142.43"]
  ttl = "${var.ttl}"
}
