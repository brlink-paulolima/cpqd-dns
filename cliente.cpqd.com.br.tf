resource "aws_route53_zone" "cliente-cpqd-com-br" { name = "cliente.cpqd.com.br" }

resource "aws_route53_record" "mx-cliente-cpqd-com-br" {
  zone_id = "${aws_route53_zone.cliente-cpqd-com-br.zone_id}"
  name    = ""
  type    = "MX"
  records = [
    "1 ASPMX.L.GOOGLE.COM.","5 ALT1.ASPMX.L.GOOGLE.COM.","5 ALT2.ASPMX.L.GOOGLE.COM.","10 ASPMX2.GOOGLEMAIL.COM.","10 ASPMX3.GOOGLEMAIL.COM.","10 ASPMX4.GOOGLEMAIL.COM.","10 ASPMX5.GOOGLEMAIL.COM."]
  ttl = "3600"
}

resource "aws_route53_record" "a-cliente-cpqd-com-br" {
  zone_id = "${aws_route53_zone.cliente-cpqd-com-br.zone_id}"
  name    = ""
  type    = "A"
  records = ["177.125.142.49","177.125.142.47","177.125.142.43"]
  ttl     = "3600"
}

resource "aws_route53_record" "cname-start-cliente-cpqd-com-br" {
  zone_id = "${aws_route53_zone.cliente-cpqd-com-br.zone_id}"
  name    = "start"
  type    = "CNAME"
  records = ["ghs.google.com."]
  ttl     = "3600"
}

resource "aws_route53_record" "txt-cliente-cpqd-com-br" {
  zone_id = "${aws_route53_zone.cliente-cpqd-com-br.zone_id}"
  name    = ""
  type    = "TXT"
  records = ["v=spf1 include:aspmx.googlemail.com ~all","google-site-verification=7tx41TjDIftQppaz3pxJkwqJtlSd6c0h7H9Z07vUtfg"]
  ttl     = "3600"
}
