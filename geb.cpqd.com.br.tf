resource "aws_route53_zone" "geb-cpqd-com-br" { name = "geb.cpqd.com.br" }

resource "aws_route53_record" "a-geb-cpqd-com-br" {
  zone_id = "${aws_route53_zone.geb-cpqd-com-br.zone_id}"
  name    = ""
  type    = "A"
  records = ["177.125.142.49","177.125.142.47","177.125.142.43"]
  ttl = "${var.ttl}"
}

resource "aws_route53_record" "cname-agenda-geb-cpqd-com-br" {
  zone_id = "${aws_route53_zone.geb-cpqd-com-br.zone_id}"
  name    = "agenda"
  type    = "CNAME"
  records = ["ghs.google.com."]
  ttl = "${var.ttl}"
}

resource "aws_route53_record" "cname-docs-geb-cpqd-com-br" {
  zone_id = "${aws_route53_zone.geb-cpqd-com-br.zone_id}"
  name    = "docs"
  type    = "CNAME"
  records = ["ghs.google.com."]
  ttl = "${var.ttl}"
}

resource "aws_route53_record" "cname-inicial-geb-cpqd-com-br" {
  zone_id = "${aws_route53_zone.geb-cpqd-com-br.zone_id}"
  name    = "inicial"
  type    = "CNAME"
  records = ["ghs.google.com."]
  ttl = "${var.ttl}"
}

resource "aws_route53_record" "cname-mail-geb-cpqd-com-br" {
  zone_id = "${aws_route53_zone.geb-cpqd-com-br.zone_id}"
  name    = "mail"
  type    = "CNAME"
  records = ["ghs.google.com."]
  ttl = "${var.ttl}"
}


resource "aws_route53_record" "mx-geb-cpqd-com-br" {
  zone_id = "${aws_route53_zone.geb-cpqd-com-br.zone_id}"
  name    = ""
  type    = "MX"
  records = ["10 ASPMX2.GOOGLEMAIL.COM.","10 ASPMX3.GOOGLEMAIL.COM.","10 ASPMX4.GOOGLEMAIL.COM.","5 ALT2.ASPMX.L.GOOGLE.COM.","10 ASPMX5.GOOGLEMAIL.COM.","1 ASPMX.L.GOOGLE.COM.","5 ALT1.ASPMX.L.GOOGLE.COM."]
  ttl = "${var.ttl}"
}


resource "aws_route53_record" "txt-geb-cpqd-com-br" {
  zone_id = "${aws_route53_zone.geb-cpqd-com-br.zone_id}"
  name    = ""
  type    = "TXT"
  records = ["google-site-verification=Oz1ny3Ixc9FDOeFHM8bf4yo1WKHjBkktzHcdLmbAsLc","v=spf1 include:aspmx.googlemail.com ~all"]
  ttl = "${var.ttl}"
}
