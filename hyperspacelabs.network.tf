resource "aws_route53_zone" "hyperspacelabs-network" { name = "hyperspacelabs.network" }

resource "aws_route53_record" "a-hyperspacelabs-network" {
  zone_id = "${aws_route53_zone.hyperspacelabs-network.zone_id}"
  name    = ""
  type    = "A"
  records = ["177.125.142.43","177.125.142.47","177.125.142.49"]
  ttl = "${var.ttl}"
}

resource "aws_route53_record" "a-www-hyperspacelabs-network" {
  zone_id = "${aws_route53_zone.hyperspacelabs-network.zone_id}"
  name    = "www"
  type    = "A"
  records = ["177.125.142.43","177.125.142.47","177.125.142.49"]
  ttl = "${var.ttl}"
}

resource "aws_route53_record" "mx-hyperspacelabs-network" {
  zone_id = "${aws_route53_zone.hyperspacelabs-network.zone_id}"
  name    = ""
  type    = "MX"
  records = ["10 ASPMX.L.GOOGLE.COM.","20 ALT1.ASPMX.L.GOOGLE.COM.","20 ALT2.ASPMX.L.GOOGLE.COM.","30 ASPMX2.GOOGLEMAIL.COM.","30 ASPMX3.GOOGLEMAIL.COM."]
  ttl = "3600"
}

resource "aws_route53_record" "txt-hyperspacelabs-network" {
  zone_id = "${aws_route53_zone.hyperspacelabs-network.zone_id}"
  name    = ""
  type    = "TXT"
  records = ["google-site-verification=X_H32o0_qEdXHX7FKU2829vTer6u7LTdoynYn5TRokk"]
  ttl = "${var.ttl}"
}
